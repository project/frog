# frog module
Drupal 8 frog

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
 
 Frog is a website crawler, that allows you to crawl websites’ URLs.
 Its name is inspired to the famous screaming frog.
 
 Has been developed for the needed to check the validity of the links
 in a headless multisite website.
 
 Passing an url it is able to go throug all the internal links of the domain
 and analyze the status code of the links and keep track on which pages a link
 has been retrieved.
 
 This module still untested and have yet a poor set of feature, any help as
 ideas, suggestions and features are welcome.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/frog

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/frog

RECOMMENDED MODULES
-------------------

 * No extra module is required.

INSTALLATION
------------

 * Install as usual, see the link below for further information:

https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 

CONFIGURATION
-------------

 * Domain configuration:
  - Login as admin.
  - Go to admin/structure/frog/domain
  - Click on add button to add a new domain
  - Click on scraping from the operation list  

MAINTAINERS
-----------

Current maintainers:

 * Giuseppe Piraino (https://www.drupal.org/u/ziomizar)
 * Fernando Katsukawa (https://www.drupal.org/u/fkatsukawa)
