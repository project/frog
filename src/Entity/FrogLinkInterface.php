<?php

namespace Drupal\frog\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Link entities.
 *
 * @ingroup frog
 */
interface FrogLinkInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Get the domain ID.
   *
   * @return int
   *   The domain ID.
   */
  public function getDomainId();

  /**
   * Set the domain ID.
   *
   * @param int $domain_id
   *   The domain ID.
   */
  public function setDomainId($domain_id);

  /**
   * Get the status of the entity.
   *
   * @return bool
   *   The status of the entity.
   */
  public function getStatus();

  /**
   * Set the status of the entity.
   *
   * @param bool $status
   *   The status of the entity.
   */
  public function setStatus($status);

  /**
   * Get the status of the link.
   *
   * @return int
   *   The status of the link.
   */
  public function getLinkStatus();

  /**
   * Set the status of the link.
   *
   * @param int $link_status
   *   The status of the link.
   */
  public function setLinkStatus($link_status);

  /**
   * Get the MD5 of the page.
   *
   * @return string
   *   The MD5 of the page.
   */
  public function getMd5();

  /**
   * Set the MD5 of the page.
   *
   * @param string $data
   *   The MD5 of the page.
   */
  public function setMd5($data);

  /**
   * Get the URL of the page.
   *
   * @return string
   *   The URL of the page.
   */
  public function getUrl();

  /**
   * Set the URL of the page.
   *
   * @param string $url
   *   The URL of the page.
   */
  public function setUrl($url);

  /**
   * Get parent URL of the page.
   *
   * @return string
   *   The parent URL of the page.
   */
  public function getParentUrl();

  /**
   * Set the parent URL of the page.
   *
   * @param string $url
   *   Parent URL of the page.
   */
  public function setParentUrl($url);

  /**
   * Gets the Link creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Link.
   */
  public function getCreatedTime();

  /**
   * Sets the Link creation timestamp.
   *
   * @param int $timestamp
   *   The Link creation timestamp.
   *
   * @return \Drupal\drupal_frog\Entity\LinkInterface
   *   The called Link entity.
   */
  public function setCreatedTime($timestamp);

}
