<?php

namespace Drupal\frog\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the ParentLinks entity.
 *
 * @ingroup frog
 *
 * @ContentEntityType(
 *   id = "frog_parent_links",
 *   label = @Translation("ParentLinks"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *   },
 *   base_table = "frog_parent_links",
 *   translatable = FALSE,
 *   admin_permission = "administer link entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 * )
 */
class FrogParentLinks extends ContentEntityBase implements FrogParentLinksInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * Verify if link already exists in database.
   *
   * @param object $parent
   *   Parent.
   * @param object $link
   *   Link.
   *
   * @return mixed
   *   Parent link.
   */
  public static function alreadyExists($parent, $link) {
    $result = \Drupal::service('entity_type.manager')->getStorage('frog_parent_links')->loadByProperties([
      'parent_id' => $parent->id(),
      'child_id' => $link->id(),
    ]);
    $parent_link = reset($result);
    return $parent_link;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentId() {
    return $this->get('parent_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentId($parent_id) {
    $this->set('parent_id', $parent_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChildId() {
    return $this->get('child_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChildId($child_id) {
    $this->set('child_id', $child_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['parent_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent URL ID'))
      ->setDescription(t('The ID of parent URL.'))
      ->setSetting('target_type', 'frog_link')
      ->setRequired(TRUE)
      ->setRevisionable(FALSE);

    $fields['child_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Child URL ID'))
      ->setDescription(t('The ID of child URL.'))
      ->setSetting('target_type', 'frog_link')
      ->setRequired(TRUE)
      ->setRevisionable(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
