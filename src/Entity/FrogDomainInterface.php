<?php

namespace Drupal\frog\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Domain entities.
 *
 * @ingroup frog
 */
interface FrogDomainInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Domain name.
   *
   * @return string
   *   Name of the Domain.
   */
  public function getName();

  /**
   * Sets the Domain name.
   *
   * @param string $name
   *   The Domain name.
   *
   * @return \Drupal\drupal_frog\Entity\FrogDomainInterface
   *   The called Domain entity.
   */
  public function setName($name);

  /**
   * Gets the Domain url.
   *
   * @return string
   *   Url of the Domain.
   */
  public function getUrl();

  /**
   * Sets the Domain url.
   *
   * @param string $url
   *   The Domain url.
   *
   * @return \Drupal\drupal_frog\Entity\FrogDomainInterface
   *   The called Domain entity.
   */
  public function setUrl($url);

  /**
   * Gets the Domain creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Domain.
   */
  public function getCreatedTime();

  /**
   * Sets the Domain creation timestamp.
   *
   * @param int $timestamp
   *   The Domain creation timestamp.
   *
   * @return \Drupal\drupal_frog\Entity\FrogDomainInterface
   *   The called Domain entity.
   */
  public function setCreatedTime($timestamp);

}
