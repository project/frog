<?php

namespace Drupal\frog\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Link entity.
 *
 * @ingroup frog
 *
 * @ContentEntityType(
 *   id = "frog_link",
 *   label = @Translation("Frog Link"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *   },
 *   base_table = "frog_link",
 *   translatable = FALSE,
 *   admin_permission = "administer link entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/frog/link/{frog_link}",
 *     "add-form" = "/admin/structure/frog/link/add",
 *     "edit-form" = "/admin/structure/frog/link/{frog_link}/edit",
 *     "delete-form" = "/admin/structure/frog/link/{frog_link}/delete",
 *     "collection" = "/admin/structure/frog/link",
 *   },
 * )
 */
class FrogLink extends ContentEntityBase implements FrogLinkInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * Load frog link by url.
   *
   * @param string $url
   *   Url.
   *
   * @return mixed
   *   Frog link if loaded, FALSE otherwise.
   */
  public static function loadByUrl($url) {
    $result = \Drupal::service('entity_type.manager')->getStorage('frog_link')->loadByProperties(['url' => $url]);
    $link = reset($result);
    return $link;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainId() {
    return $this->get('domain_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDomainId($domain_id) {
    $this->set('domain_id', $domain_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinkStatus() {
    return $this->get('link_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLinkStatus($link_status) {
    $this->set('link_status', $link_status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMd5() {
    return $this->get('md5')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMd5($data) {
    $this->set('md5', $data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return $this->get('url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUrl($url) {
    $this->set('url', $url);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentUrl() {
    return $this->get('parent_url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentUrl($url) {
    $this->set('parent_url', $url);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastChecked() {
    return $this->get('last_checked')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastChecked($timestamp) {
    $this->set('last_checked', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Domain'))
      ->setDescription(t('The name of the Domain entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['domain_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Domain ID'))
      ->setDescription(t('The domain that this url belongs.'))
      ->setSetting('target_type', 'frog_domain')
      ->setRequired(TRUE)
      ->setRevisionable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Link is published.'));

    $fields['link_status'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Link status'))
      ->setDescription(t('The status of the link.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'integer',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['md5'] = BaseFieldDefinition::create('string')
      ->setLabel(t('MD5'))
      ->setDescription(t('MD5 of downloaded page. Used to check changes.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('URL'))
      ->setDescription(t('The URL of the Link entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 2000)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['parent_url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent URL'))
      ->setDescription(t('The parent URL of given URL, if it have.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 2000);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['last_checked'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Last checked'))
      ->setDescription(t('Last time that was checked that domain/page.'));

    return $fields;
  }

}
