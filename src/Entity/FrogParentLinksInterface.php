<?php

namespace Drupal\frog\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Parent entities.
 *
 * @ingroup frog
 */
interface FrogParentLinksInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Get the parent Id.
   *
   * @return int
   *   The parent Id.
   */
  public function getParentId();

  /**
   * Set the parent Id.
   *
   * @param int $parent_id
   *   The parent Id.
   */
  public function setParentId($parent_id);

  /**
   * Get the child Id.
   *
   * @return int
   *   The child Id.
   */
  public function getChildId();

  /**
   * Set the child Id.
   *
   * @param int $child_id
   *   The child Id.
   */
  public function setChildId($child_id);

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Parent creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Parent.
   */
  public function getCreatedTime();

  /**
   * Sets the Parent creation timestamp.
   *
   * @param int $timestamp
   *   The Parent creation timestamp.
   *
   * @return \Drupal\drupal_frog\Entity\FrogParentLinksInterface
   *   The called Parent entity.
   */
  public function setCreatedTime($timestamp);

}
