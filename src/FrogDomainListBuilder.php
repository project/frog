<?php

namespace Drupal\frog;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Frog Domain entities.
 *
 * @ingroup drupal_frog
 */
class FrogDomainListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Domain ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
     * @var \Drupal\frog\Entity\FrogDomain $entity
     */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.frog_domain.canonical',
      ['frog_domain' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
