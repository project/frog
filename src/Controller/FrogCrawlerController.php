<?php

namespace Drupal\frog\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\frog\Entity\FrogDomain;
use Drupal\frog\Entity\FrogLink;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\frog\FrogLinkDownloaderInterface;

/**
 * Class FrogCrawlerController.
 *
 * Frog Crawler Controller.
 */
class FrogCrawlerController extends ControllerBase {

  /**
   * Drupal\frog\LinkDownloaderInterface definition.
   *
   * @var \Drupal\frog\FrogLinkDownloaderInterface
   */
  protected $drupalFrogLinkDownloader;

  /**
   * FrogCrawlerController constructor.
   *
   * @param \Drupal\frog\FrogLinkDownloaderInterface $frog_link_downloader
   *   Frog link downloader.
   */
  public function __construct(FrogLinkDownloaderInterface $frog_link_downloader) {
    $this->drupalFrogLinkDownloader = $frog_link_downloader;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('frog.link_downloader')
    );
  }

  /**
   * Scraping.
   *
   * @param \Drupal\frog\Entity\FrogDomain $frog_domain
   *   The domain.
   *
   * @return string
   *   Return Hello string.
   */
  public function scraping(FrogDomain $frog_domain) {
    $batch = [
      'title' => t('Scraping links...'),
      'operations' => [
        [
          [__CLASS__, 'checkDomain'],
          [$frog_domain],
        ],
      ],
      'finished' => [__CLASS__, 'scrapingFinishedCallback'],
      'init_message' => t('Preparing to scrap links...'),
    ];

    batch_set($batch);

    return batch_process('/admin/structure/frog/domain');
  }

  /**
   * Check domain.
   *
   * @param \Drupal\frog\Entity\FrogDomain $domain
   *   The domain.
   * @param array $context
   *   Batch context.
   */
  public static function checkDomain(FrogDomain $domain, array &$context) {
    $links = FrogCrawlerController::getLinks($domain);

    if (!isset($context['sandbox']['links'])) {
      $context['sandbox']['links'] = [];
      $context['results'] = [];
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current'] = 0;
    }

    // Without any links, the domain became the first link used to start
    // scraping.
    if (!count($links)) {
      // Save the link with all the info updated.
      FrogCrawlerController::check($domain->getUrl(), $domain, $domain->getUrl());
      $context['sandbox']['links'][$domain->getUrl()] = TRUE;
      $context['sandbox']['progress']++;
      $context['sandbox']['current']++;
      $context['message'] = 'Checking link ' . $domain->getUrl();
      FrogCrawlerController::setProgressMessage($context, $domain->getUrl());
    }
    else {
      $max_check_batch = 2;
      $current = 1;
      foreach ($links as $link) {
        $link = FrogLink::load($link);
        if (isset($context['sandbox']['links'][$link->getURL()]) && $context['sandbox']['links'][$link->getURL()]) {
          continue;
        }

        FrogCrawlerController::check($link->getUrl(), $domain);
        $context['sandbox']['links'][$link->getUrl()] = TRUE;
        $context['sandbox']['progress']++;
        $context['sandbox']['current']++;
        FrogCrawlerController::setProgressMessage($context, $link->getURL());
        if ($current == $max_check_batch) {
          break;
        }

        $current++;
      }
    }

    // Get all the links in the database to check if there are links that need
    // to be downloaded.
    // Consider that all the link downloaded has been passed as $link parameter
    // to the check() method and are saved into the $context['links'][$link] as
    // TRUE.
    $links = FrogCrawlerController::getLinks($domain);
    foreach ($links as $link) {
      $link = FrogLink::load($link);
      if (!isset($context['sandbox']['links'][$link->getUrl()])) {
        $context['sandbox']['links'][$link->getUrl()] = FALSE;
      }
    }

    // Check if all the links has been checked.
    // Consider that all the link downloaded has been passed as $link parameter
    // to the check() method and are saved into the $context['links'][$link] as
    // TRUE.
    $context['finished'] = TRUE;
    foreach ($context['sandbox']['links'] as $link) {
      if (!$link) {
        $context['finished'] = FALSE;
      }
    }

    // Update the amount pof links retrieved by the crawler.
    $context['results'] = $context['sandbox']['links'];
  }

  /**
   * Scraping finished batch callback.
   *
   * @param bool $success
   *   Flag the indicated if batch has no errors.
   * @param array $results
   *   Results.
   * @param array $operations
   *   Operations.
   */
  public static function scrapingFinishedCallback($success, array $results, array $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One post processed.', '@count posts processed.'
      );
      \Drupal::logger('frog')->notice($message);
    }
    else {
      $message = t('Finished with an error.');
      \Drupal::logger('frog')->error($message);
    }
  }

  /**
   * Set progress message.
   *
   * @param mixed $context
   *   Context.
   * @param string $link_url
   *   Link url.
   */
  public static function setProgressMessage(&$context, $link_url) {
    $total_count = count($context['results']);
    $total = $total_count ? $total_count : 1;
    $percentage = round($context['sandbox']['current'] / $total * 100, 2);
    $context['message'] = t('Checking @current of @total (@percentage%). Link: @link', [
      '@current' => $context['sandbox']['current'],
      '@total' => $total,
      '@link' => $link_url,
      '@percentage' => $percentage,
    ]);
  }

  /**
   * Check, validate and save the link.
   *
   * @param string $link
   *   The link.
   * @param object $domain
   *   The domain.
   * @param string $parent_url
   *   The parent url.
   *
   * @return mixed
   *   Array of links, FALSE otherwise.
   */
  public static function check($link, $domain, $parent_url = '') {
    $link = FrogCrawlerController::prepareUrl($link, $domain);

    if (!UrlHelper::isValid($link, UrlHelper::isExternal($link))) {
      return FALSE;
    }

    if (!filter_var($link, FILTER_VALIDATE_URL)) {
      return FALSE;
    }

    // Save the link with all the info updated.
    \Drupal::service('frog.link_downloader')->save($link, $domain, $parent_url);

    // Just save children links for the current domain.
    if (strpos($link, $domain->getUrl()) !== FALSE) {
      // Check if the link downloaded contain other links.
      $links = \Drupal::service('frog.link_downloader')->extractLinks();
      FrogCrawlerController::saveChildrenLinks($links, $domain, $link);
      return $links;
    }
  }

  /**
   * Get a list domains.
   *
   * @return array
   *   A list of domains.
   */
  public function getDomains() {
    $query = \Drupal::entityQuery('frog_domain');
    // @todo filter by older than x days or without an md5 version of the page.
    $domains = $query->execute();
    return $domains;
  }

  /**
   * Get a list of links.
   *
   * @param object $domain
   *   Domain.
   *
   * @return array
   *   List of links.
   */
  public static function getLinks($domain) {
    // Get all Link entities.
    $query = \Drupal::entityQuery('frog_link');
    // @todo filter by older than x days or without an md5 version of the page.
    $query->condition('domain_id', $domain->id());
    $links = $query->execute();
    return $links;
  }

  /**
   * Prepare url.
   *
   * @param string $url
   *   Requested Url.
   * @param object $domain
   *   Requested Domain.
   *
   * @return string
   *   Prepared url.
   */
  public static function prepareUrl($url, $domain) {
    if ($url === '' || $url === $domain->getUrl()) {
      return $domain->getUrl();
    }

    // Create a new link with the url and the status code.
    if (!UrlHelper::isExternal($url)) {
      if (strpos($url, '/') === FALSE) {
        // Then the url is relative and need to be completed with the domain.
        $url = '/' . $url;
      }

      $url = rtrim($domain->getUrl(), '/') . $url;
    }

    return $url;
  }

  /**
   * Save children links.
   *
   * @param array $links
   *   The children links.
   * @param object $domain
   *   The domain.
   * @param string $parent_url
   *   The parent url, if exists.
   */
  public static function saveChildrenLinks(array $links, $domain, $parent_url) {
    foreach ($links as $url) {
      $url = FrogCrawlerController::prepareUrl($url, $domain);

      if (UrlHelper::isValid($url)) {
        \Drupal::service('frog.link_downloader')->save($url, $domain, $parent_url, FALSE);
      }
    }
  }

}
