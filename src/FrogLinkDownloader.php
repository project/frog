<?php

namespace Drupal\frog;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\frog\Entity\FrogLink;
use Drupal\frog\Entity\FrogParentLinks;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Client;

/**
 * Class LinkDownloader.
 */
class FrogLinkDownloader implements FrogLinkDownloaderInterface {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The crawler.
   *
   * @var \Symfony\Component\DomCrawler\Crawler
   */
  protected $crawler;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new LinkDownloader object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   A Guzzle client object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(Client $http_client, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $http_client;
    $this->entityTypeManager = $entity_type_manager;
    $this->crawler = new Crawler();
  }

  /**
   * Save the link.
   *
   * @param string $url
   *   The Url of the link to save.
   * @param object $domain
   *   The domain that link belongs.
   * @param string $parent_url
   *   The parent url of the link.
   * @param bool $download
   *   Download parameter to verify when the page
   *   must to be downloaded.
   *
   * @throws \Exception
   */
  public function save($url, $domain, $parent_url = '', $download = TRUE) {
    $html_compressed = '';
    $status_code = '';

    if ($download) {
      try {
        // Client take few seconds to complete based on the speed of the
        // resource.
        // @todo Speed this up asking just the status for external links.
        $response = $this->httpClient->get($url);
        $status_code = $response->getStatusCode();

        // Just when the status code is 200 we try to search and scrap other
        // links.
        if ($response->getStatusCode() == 200) {
          // StreamInterface Returns the body as a stream.
          $this->crawler->clear();
          $this->crawler->addHtmlContent($response->getBody()->getContents());
          $body = $response->getBody()->__toString();
          $html_compressed = md5($response->getBody()->__toString());
        }
      }
      catch (\Exception $e) {
        $html_compressed = TRUE;
        $status_code = $e->getCode() != 0 ? $e->getCode() : -1;
      }
    }

    $result = $this->entityTypeManager->getStorage('frog_link')->loadByProperties(['url' => $url]);
    $link = reset($result);

    // Check if link exists, and if exists, check the MD5 of the page.
    if ($link) {
      // If MD5 is different, so update the link.
      if (trim($html_compressed) != '' && $html_compressed !== $link->getMd5()) {
        $link->setUrl($url);
        $link->setLastChecked(time());
        $link->setMd5($html_compressed);
        $link->setLinkStatus($status_code);
      }
    }
    // Otherwise, create a new link.
    else {
      $link = FrogLink::create([
        'domain_id' => $domain->id(),
        'url' => $url,
        'md5' => $html_compressed,
        'link_status' => $status_code,
        'last_checked' => time(),
      ]);
    }

    $link->save();

    if ($parent_url === '') {
      return FALSE;
    }

    $parentLink = FrogLink::loadByUrl($parent_url);

    if ($parentLink && $link && !FrogParentLinks::alreadyExists($parentLink, $link)) {
      $parent = FrogParentLinks::create([
        'parent_id' => $parentLink->id(),
        'child_id' => $link->id(),
      ]);

      $parent->save();
    }
  }

  /**
   * Extract links.
   *
   * @return mixed
   *   Extracted links.
   */
  public function extractLinks() {
    // Search inside this link.
    $links = $this->crawler->filter('a')->extract(['href']);
    return $links;
  }

}
